#ifndef FUNCTIONS
#define FUNCTIONS
#include <tuple>

namespace write {
	void FillMatrix(int arr[][4], const int X, const int Y);

	std::tuple<int, int> FindNum(int arr[][4], const int X, const int Y);

	int SummDigit(int num);

	void OrderMatrix(int arr[][4], const int X, const int Y);

	void PrintMatrix(int arr[][4], const int X, const int Y);
}

#endif
