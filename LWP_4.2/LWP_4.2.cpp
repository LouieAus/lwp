﻿#include <iostream>
using namespace std;

int main() {
	int n;
	int arr_j;
	cout << "Enter number of array quantity: ";
	cin >> n;
	unsigned int* arr = new unsigned int[n];

	cout << "Enter value for each element:" << '\n';

	//Вводим значения
	for (int i = 0; i < n; i++) {
		cout << i + 1 << "/" << n << ": ";
		cin >> arr[i];
	}

	//Сортируем элементы по условиям
	for (int f = 0; f < n; f++) {
		for (int j = 0; j < n - 1; j++) {
			int a = arr[j];
			int b = arr[j + 1];
			int sum_a = a % 10;
			int sum_b = b % 10;
			int min_a = sum_a;
			int min_b = sum_b;

			while (a / 10 != 0) { //разбиваем число на цифры и суммируем их
				a = a / 10;
				if (a % 10 < min_a) min_a = a % 10;
				sum_a = sum_a + (a % 10);
			}

			while (b / 10 != 0) { //разбиваем последующее число на цифры и суммируем их
				b = b / 10;
				if (b % 10 < min_b) min_b = b % 10;
				sum_b = sum_b + (b % 10);
			}

			if (sum_a > sum_b) { //сравниваем суммы цифр
				arr_j = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = arr_j;
			}
			else if (sum_a == sum_b) {
				if (min_a > min_b) { //сравниваем минимальные цифры чисел
					arr_j = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = arr_j;
				}
				else if (min_a == min_b) {
					if (arr[j] > arr[j + 1]) { //сравниваем сами числа
						arr_j = arr[j];
						arr[j] = arr[j + 1];
						arr[j + 1] = arr_j;
					}
				}
			}
		}
	}

	//Выводим массив
	cout << '\n' << "Final array:" << '\n';
	for (int k = 0; k < n; k++) {
		cout << k + 1 << "/" << n << ": " << arr[k] << '\n';
	}

	delete[] arr;
	system("pause");
	return 0;
}
