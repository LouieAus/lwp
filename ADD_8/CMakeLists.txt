cmake_minimum_required(VERSION 3.22 FATAL_ERROR)

project(ADD_8)

include_directories(headers)

set(SOURCES ADD_8.cpp figure.cpp headers/figure.h)

add_executable(ADD_8 ${SOURCES})