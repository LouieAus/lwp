#include <iostream>
#include <tuple>
#include "functions.h"

namespace write{
	void FillMatrix(int arr[][4], const int X, const int Y) {
		for (int i = 0; i < X; i++)
			for (int j = 0; j < Y; j++) {
				int num;
				std::cout << "[" << i + 1 << ", " << j + 1 << "]: ";
				std::cin >> num;
				std::cout << '\n';
				arr[i][j] = num;
			}
	}

	std::tuple<int, int> FindNum(int arr[][4], const int X, const int Y) {
		int minNum = arr[0][0];
		int maxNum = arr[0][0];
		for (int i = 0; i < X; i++)
			for (int j = 0; j < Y; j++) {
				if (arr[i][j] < minNum) minNum = arr[i][j];
				else if (arr[i][j] > maxNum) maxNum = arr[i][j];
			}
		return std::make_tuple(minNum, maxNum);
	}

	int SummDigit(int num) {
		if (num < 0) num *= -1;
		int sum = 0;
		while (num != 0) {
			sum += num % 10;
			num = num / 10;
		}
		return sum;
	}

	void OrderMatrix(int arr[][4], const int X, const int Y) {
		for (int i = 0; i < X; i++) {
			for (int k = 0; k < Y; k++) {
				for (int j = 0; j < Y - 1; j++) {
					if (arr[i][j] > arr[i][j + 1]) {
						int n = arr[i][j + 1];
						arr[i][j + 1] = arr[i][j];
						arr[i][j] = n;
					}
				}
			}
		}
	}

	void PrintMatrix(int arr[][4], const int X, const int Y) {
		for (int i = 0; i < X; i++) {
			std::cout << '\n' << i + 1 << ": ";
			for (int j = 0; j < Y; j++)
				std::cout << arr[i][j] << ", ";
		}
		std::cout << '\n';
	}
}
