#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace forms {
	struct size {
		float wide, high;
	};
	struct coord {
		float x, y;
	};
	struct color {
		int r, g, b;
	};
	class Rectangles {
	private:
		size rectSize; //������
		coord rectCoord; //����������
		color rectColor; //����
		float rectSpeed; //��������
	public:
		sf::RectangleShape rect;
		Rectangles(size Size, coord Coord, color Color, float Speed); //�����������
		void RectInit(size Size, coord Coord, color Color, float Speed, sf::RectangleShape& rectan); //�������������
		void PosChange(); //������� ����� ������� (������� ��������)
		sf::RectangleShape Create(size S, coord C, color Cl, float Sp); //������� �������� ��������������
	};
}