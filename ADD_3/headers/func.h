#include <fstream>
#include <string>
#include <iostream>

namespace processing {
	std::string ReadFile(std::string path);

	void Operation(std::string &text);

	void Print(std::string &text);
}
