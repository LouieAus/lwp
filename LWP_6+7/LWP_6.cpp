﻿#include <iostream>
#include "functions.h"


int main(int argc, char** argv) {
	const int x = 2;
	const int y = 4;

	int minN, maxN;

	int arr[x][y];

	write::FillMatrix(arr, x, y); //Заполняем матрицу

	std::tie(minN, maxN) = operation::FindNum(arr, x, y); //Ищем минимальное и максимальное числа

	if (operation::SummDigit(minN) == operation::SummDigit(maxN)) //Сортируем матрицу
		operation::OrderMatrix(arr, x, y);

	read::PrintMatrix(arr, x, y); //Выводим матрицу

	system("pause");
	return 0;
}