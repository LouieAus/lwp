﻿#include <iostream>
using namespace std;

int main()
{
    int n;
    int rem;
    unsigned int num;
    unsigned int temp_rem;
    int count = 0;
    unsigned int arr_j;

    cout << "Enter number of array quantity: ";
    cin >> n;
    unsigned int* arr = new unsigned int[n];

    cout << "Enter value for each element:" << '\n';

    //Вводим значения и параллельно ищем необходимые числа
    for (int i = 0; i < n; i++) {
        cout << i + 1 << "/" << n << ": ";
        cin >> arr[i];
        num = arr[i];
        bool ok = 0;
        while (num / 10 != 0) {
            ok = 1;
            rem = num % 10;
            num = num / 10;
            if (num % 10 != rem) {
                ok = 0;
                break;
            }
        }
        if (ok) count++;
    }

    //Сортируем массив по неубыванию, если условие выполнено
    if (count >= 3) {
        for (int k = 0; k < n; k++) {
            for (int j = 0; j < n - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    arr_j = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = arr_j;
                }
            }
        }
    }

    //Вывод массива
    cout << "Final array: " << '\n';
    for (int f = 0; f < n; f++) cout << f + 1 << "/" << n << ": " << arr[f] << '\n';

    system("pause");
    delete[] arr;
    return 0;
}
