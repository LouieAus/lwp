﻿#include <iostream>
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <typeinfo>

using namespace std::chrono_literals;

int main() {

	std::chrono::milliseconds time = 10ms; //delay

	int k = 2; //ratio of window size

	int winX = k*300;
	int winY = k*100;

	int rect[3][5] = {{k*20, k*40, 0, 0, -4},
					  {k*25, k*70, 0, 0, -2},
					  {k*30, k*50, 0, 0, -3} }; //wide, high, x, y, speed

	int emptySize = (winX - rect[0][0] - rect[1][0] - rect[2][0]) / 4;

	sf::RenderWindow window(sf::VideoMode(winX, winY), L"Programm");

	sf::RectangleShape rect1(sf::Vector2f(rect[0][0], rect[0][1]));
	rect1.setFillColor(sf::Color(20, 200, 10));
	rect[0][2] = emptySize; // X-coordinate of rect1
	rect[0][3] = winY - (rect[0][1]); // Y-coordinate of rect1
	rect1.setPosition(rect[0][2], rect[0][3]);

	sf::RectangleShape rect2(sf::Vector2f(rect[1][0], rect[1][1]));
	rect2.setFillColor(sf::Color(200, 20, 10));
	rect[1][2] = rect[0][2] + rect[0][0] + emptySize; // X-coordinate of rect2
	rect[1][3] = winY - (rect[1][1]); // Y-coordinate of rect2
	rect2.setPosition(rect[1][2], rect[1][3]);

	sf::RectangleShape rect3(sf::Vector2f(rect[2][0], rect[2][1]));
	rect3.setFillColor(sf::Color(20, 10, 200));
	rect[2][2] = rect[1][2] + rect[1][0] + emptySize; // X-coordinate of rect3
	rect[2][3] = winY - (rect[2][1]); // Y-coordinate of rect3
	rect3.setPosition(rect[2][2], rect[2][3]);

	while(window.isOpen()) {
		sf::Event event;
		if (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		for (int i = 0; i < 3; i++) { //rectangles moving
			if (rect[i][3] > 0) {
				rect[i][3] += rect[i][4];
			}
		}

		rect1.setPosition(rect[0][2], rect[0][3]);
		rect2.setPosition(rect[1][2], rect[1][3]);
		rect3.setPosition(rect[2][2], rect[2][3]);

		window.clear(sf::Color::Black);
		window.draw(rect1);
		window.draw(rect2);
		window.draw(rect3);
		window.display();

		std::this_thread::sleep_for(time);
	}

	return 0;
}