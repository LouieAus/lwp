#include <iostream>
#include "functions.h"
using namespace std;

void operation::yearDefine(int year)
{
	bool vis_year = 0;
	if (year >= 2016)
		cout << year << '\n';
	else {
		vis_year = VisYearDefine(year);

		if (vis_year) {
			while (year < 2016)
				year += 28;
			cout << year << '\n';
		}
		else {
			int year_kopy = year;
			while (year_kopy < 2016)
				year_kopy += 6;
			while (year < 2016)
				year += 11;
			if ((year_kopy < year) && (VisYearDefine(year_kopy) == 0))
				cout << year_kopy << '\n';
			else
				cout << year << '\n';
		}
	}
}
bool operation::VisYearDefine(int year) {
	if ((year % 4 == 0) && (year % 100 != 0)) {
		return 1;
	}
	else if (year % 400 == 0)
		return 1;
	else
		return 0;
}

