﻿#include <iostream>
#include <math.h>
#include <vector>

namespace operation {
	bool Check(int num) { //ф-ция проверки числа на наличие 0 и отсутсвтие 1
		bool first = 0;
		bool second = 1;
		while (num != 0) {
			if (num % 10 == 0)
				first = 1;
			else if (num % 10 == 1)
				second = 0;
			num = num / 10;
		}
		if (first && second) {
			return false;
		}
		else {
			return true;
		}
	}
}
int main()
{
	std::vector<int> arr(5);
	for (int k = 0; k < arr.size(); k++) { //заполнение массива
		//arr[k] = std::rand() % 3000;
		std::cin >> arr[k];
	}
	for (int j = 0; j < arr.size(); j++) {
		float sqrtNumm = sqrt(arr[j]);
		if (!operation::Check(arr[j])) { //проверка числа на наличие 0 и отсутсвтие 1
			for (int i = j; i < arr.size() - 1; i++) { //удаление элемента
				arr[i] = arr[i + 1];
			}
			j--;
			arr.resize(arr.size() - 1);
		}
		else if (sqrtNumm == (int)sqrtNumm) { //проверка числа на квадрат целого числа
			arr.resize(arr.size() + 1);
			for (int i = arr.size() - 1; i > j; i--) { //дублирование числа
				arr[i] = arr[i - 1];
			}
			arr[j] = arr[j + 1];
			j++;
		}
	}

	std::cout << "_____________" << '\n';
	for (auto &a: arr) { //вывод массива
		std::cout << a << '\n';
	}
}
