﻿#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "Rus");

	int n;
	int maximal = 0;
	cin >> n;

	int* arr = new int[n];

	for (int i = 0; i < n; i++) {
		int num;
		cin >> num;
		arr[i] = num;
	}

	for (int i = 0; i < n; i++) {
		if (arr[i] % 5 != 0) {
			for (int j = i + 1; j < n; j++) {
				if ((arr[j] % 5 != 0) && (i != j)) {

					cout << arr[i] * arr[j] << ": наибольшее ";
					if (arr[i] > arr[j]) cout << arr[i] << ", номер в последовательности " << i + 1;
					else cout << arr[j] << ", номер в последовательности " << j + 1;
					cout << '\n';

				}
			}

		}
	}
}