﻿#include <iostream>
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <typeinfo>
#include <vector>
#include "class.h"

using namespace std::chrono_literals;

int main() {
	int winX = 150;
	int winY = 300;
	sf::RenderWindow window(sf::VideoMode(winX, winY), L"Programm");

	std::vector<forms::Rectangles*> figures;

	//Данные для 1 прямоугольника
	forms::size sizeRect1{ 25, 70}; //размер
	forms::coord coordRect1{ 20, winY - (sizeRect1.high + 1) }; //координаты
	forms::color colorRect1{ 200, 10, 20 }; //цвет

	//Данные для 2 прямоугольника
	forms::size sizeRect2{ 30, 60 }; 
	forms::coord coordRect2{ 60, winY - (sizeRect2.high + 1) };
	forms::color colorRect2{ 10, 200, 20 };

	//Данные для 3 прямоугольника
	forms::size sizeRect3{ 20, 50 }; 
	forms::coord coordRect3{ 110, winY - (sizeRect3.high + 1) };
	forms::color colorRect3{ 10, 20, 200 };

	//объявление и создание прямоугольников
	forms::Rectangles rect1(sizeRect1, coordRect1, colorRect1, -3); figures.push_back(&rect1);
	forms::Rectangles rect2(sizeRect2, coordRect2, colorRect2, -1); figures.push_back(&rect2);
	forms::Rectangles rect3(sizeRect3, coordRect3, colorRect3, -2); figures.push_back(&rect3);

	while (window.isOpen()) {
		sf::Event event;
		if (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear(sf::Color::Black);

		//движение прямоугольников
		for (auto figure : figures) {
			figure->PosChange();
			window.draw(figure->rect);
		}
		window.display();

		std::this_thread::sleep_for(30ms);
	}
}