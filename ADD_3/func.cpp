#include <fstream>
#include <string>
#include <iostream>
#include "func.h"

namespace processing {
	std::string ReadFile(std::string path) {
		std::string line;
		std::ifstream in(path);
		if (in.is_open())
			getline(in, line);
		in.close();
		return line;
	}

	void Operation(std::string &text) {
		char letters[6] = { 'a', 'e', 'i', 'o', 'u', 'y' };
		char upLetters[6] = { 'A', 'E', 'I', 'O', 'I', 'Y' };
		for (auto& letter : text) {
			for (int i = 0; i < 6; i++){
				if (letter == letters[i])
					letter = upLetters[i];
			}
		}
	}

	void Print(std::string &text) {
		std::cout << text;
	}
}