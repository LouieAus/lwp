#include "vector.h"
namespace vector {

	Vector::Vector(int arrSize, std::string arrType) {
		Init(arrSize, arrType);
	}

	Vector::~Vector() {
		if (type == "int")
			delete[] Vector_int;
		else if (type == "float")
			delete[] Vector_float;
		else {
			delete[] Vector_char;
		}
	}

	void Vector::Init(int arrSize, std::string arrType) {
		if ((arrSize >= 0) && (type == "int" || type == "float" || type == "char")) {
			size = arrSize;
			type = arrType;
		}
		else {
			std::cout << "Wrong data! Size = 1 and type = int." << '\n';
			size = 1;
			type = "int";
		}
		if (type == "int") {
			std::cout << "Yes1" << '\n';
			Vector_int = new int[size];
		}
		else if (type == "float")
			Vector_float = new float[size];
		else {
			Vector_char = new char[size];
		}
	}

	int Vector::GetSize() {
		return size;
	}

	void Vector::Add(int INT, float FLOAT, char CHAR, int index) {
		size++;
		if (index == -1)
			index = size - 1;
		if (type == "int") {
			std::cout << "Yes2" << '\n';
			int* newVector_int = new int[size];
			for (int i = 0; i < index; i++)
				newVector_int[i] = Vector_int[i];
			delete[] Vector_int;
			newVector_int[index] = INT;
			for (int k = index + 1; k < size; k++)
				newVector_int[k] = Vector_int[k-1];
			Vector_int = newVector_int;
			delete[] newVector_int;
		}
		else if (type == "float") {
			float* newVector_float = new float[size];
			for (int i = 0; i < index; i++)
				newVector_float[i] = Vector_float[i];
			delete[] Vector_float;
			newVector_float[index] = FLOAT;
			for (int k = index + 1; k < size; k++)
				newVector_float[k] = Vector_float[k - 1];
			Vector_float = newVector_float;
			delete[] newVector_float;
		}
		else {
			char* newVector_char = new char[size];
			for (int i = 0; i < index; i++)
				newVector_char[i] = Vector_char[i];
			delete[] Vector_char;
			newVector_char[index] = CHAR;
			for (int k = index + 1; k < size; k++)
				newVector_char[k] = Vector_char[k - 1];
			Vector_char = newVector_char;
			delete[] newVector_char;
		}
	}

	int Vector::GetItem_Int(int index) {
		if (type == "int")
			return Vector_int[index];
	}

	bool Vector::Check_Int() {
		bool check = 1;
		for (int j = 0; j < size-1; j++) {
			if (Vector_int[j] > Vector_int[j + 1])
				check = 0;
			break;
		}
		return check;
	}
}