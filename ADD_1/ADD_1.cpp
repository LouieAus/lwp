﻿//Tmax = 2 sec
//Vmax = 256 Mb

#include <iostream>
using namespace std;

int main()
{                      //Q1,P1,Q2,P2,A
    short int data[5] = {0, 0, 0, 0, 0};

    for (short int i = 0; i < 5; i++)
        cin >> data[i];

    short int lkw_num[2] = { 0, 0 };

    if (data[0] > data[2]) {
        lkw_num[0] = 2;
        lkw_num[1] = 0;
    }
    else {
        lkw_num[0] = 0;
        lkw_num[1] = 2;
    }
    
    int prise = 0;
    short int weight = data[4];

    while ((weight - data[lkw_num[1]]) > 0) {
        weight -= data[lkw_num[1]];
        prise += data[lkw_num[1] + 1];
    }

    int maxLKW_prise = prise + data[lkw_num[1] + 1];

    while (weight > 0) {
        weight -= data[lkw_num[0]];
        prise += data[lkw_num[0] + 1];
    }

    if (maxLKW_prise < prise)
        cout << maxLKW_prise;
    else
        cout << prise;
}
