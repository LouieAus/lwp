#include <iostream>
#include <fstream>
#include <string>

bool VisYearDefine(int year) {
	if ((year % 4 == 0) && (year % 100 != 0)) {
		return 1;
	}
	else if (year % 400 == 0)
		return 1;
	else
		return 0;
}
int main()
{
	int year;
	char symbol = ' ';
	std::string line;

	std::ifstream input("calendar.txt");

	if (input.is_open()) {
		getline(input, line);
		int quantity = stoi(line);
		for (int i = 0; i < quantity; i++) {
			getline(input, line);
			year = stoi(line.substr(line.rfind(symbol)));
			while (year < 2016) {
				if (VisYearDefine(year)) {
					year += 28;
				}
				else if (VisYearDefine(year - 1)) {
					year += 6;
				}
				else {
					year += 11;
				}
			}
			std::cout << year << '\n';
		}
	}
}
