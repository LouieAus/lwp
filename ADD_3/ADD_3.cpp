﻿#include <iostream>
#include "func.h"

int main()
{
    std::string stroke = processing::ReadFile("stroke.txt");
    processing::Operation(stroke);
    processing::Print(stroke);
}
