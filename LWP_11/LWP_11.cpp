﻿#include <iostream>
#include <vector>
#include "class.h"

//датчик давления ПД-100 - давление
//дачтик температуры ПТ-100 - температура

int main() {
	setlocale(LC_ALL, "Rus");

	std::vector<Data::Applience*> applience;

	Data::Applience app_1("0001", "ПД-100", "Датчик давления", "Па"); applience.push_back(&app_1);
	Data::Applience app_2("0002", "ПТ-100", "Датчик температуры", "Град."); applience.push_back(&app_2);

	for (const auto app : applience) {
		app->Poll();
		app->Print();
	}
}