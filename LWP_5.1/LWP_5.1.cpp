﻿#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char** argv) {
	setlocale(LC_ALL, "rus");

	ifstream file("read_file.txt");
	short int strSize = 100;
	char stroke[strSize];
	file.getline(stroke, strSize);
	file.close();

	char keys[7] = { ',', '.', ':', ';', '?', '!', '-' };

	short int count = 0;
	for (short int i; i < strSize; i++) {
		for (short int key = 0; key < 7; key++) {
			if (stroke[i] == keys[key])
				count++;
		}
	}

	cout << "Количество символов: " << count << "\n";

	system("pause");
	return 0;
}
