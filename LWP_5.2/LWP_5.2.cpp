﻿#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char** argv) {
	setlocale(LC_ALL, "rus");
	char letters[33] = { 'а', 'б', 'в', 'г', 'д', 'е',
						'ё', 'ж' , 'з', 'и', 'й', 'к',
						'л', 'м', 'н', 'о', 'п', 'р',
						'с', 'т', 'у', 'ф', 'х', 'ц',
						'ч', 'ш', 'щ', 'ъ', 'ы', 'ь',
						'э', 'ю', 'я' };

	string* words = new string[100];
	short int maxLtr = 0;
	ifstream file("read_file.txt"); //открываем файл на чтение
	string line;
	short int wrdsCount = 0;

	if (file.is_open()) {
		while (getline(file, line)) {
			string word = "";
			for (short int i = 0; i < line.size(); i++) { //проверка каждого символа строки
				bool wordEnd = 1;
				char ltr = tolower(line[i]);
				for (short int k = 0; k < 33; k++) {
					if (ltr == letters[k])
						wordEnd = 0;
				}
				if (!wordEnd)
					word += ltr;
				else {
					short int ltrCount = 0;
					for (short int letterIndex = 0; letterIndex < word.size() - 1; letterIndex++) { //проверка на условие
						for (short int index = letterIndex + 1; index < word.size(); index++) {
							if (word[letterIndex] == word[index])
								ltrCount++;
						}
						if (ltrCount == 3) {
							bool foundThatWord = 0;
							for (short int ind = 0; ind < 100; ind++) { //проверка на то, что найденное слово встречается в тексте первый раз
								if (words[ind] == word)
									foundThatWord = 1;
							}
							if (!foundThatWord) { //если слово не встречалось, то добавляем его в список

								cout << "3 letters: " << word << "\n";
								if (wrdsCount != 100) { //добавляем пока список не будет полон
									words[wrdsCount] = word;
									if (word.size() > maxLtr)
										maxLtr = word.size();
									wrdsCount++;
								}
								else { //елси список полон, то ищем самое большое слово и заменяем его
									if (word.size() < maxLtr) { //если слово меньше самого большого в списке
										for (short int wrdInd = 0; wrdInd < 100; wrdInd++) {
											if (words[wrdInd].size() == maxLtr) { //если нашли самое большое, то заменяем его
												words[wrdInd] = word;
												maxLtr = 0;
											}
											else { //заново ищем размер самого большого слова из списка
												if (words[wrdInd].size() > maxLtr)
													maxLtr = words[wrdInd].size();
											}

										}
									}
								}
								break;
							}
						}
					}
					word = "";
					ltrCount = 0;
				}
			}
			cout << word << "\n";
		}
	}
	file.close();

	for (short int numCycle = 0; numCycle < 100; numCycle++) { //сортировка списка по неубыванию
		for (short int index = 0; index < 99; index++) {
			if (words[index].size() > words[index + 1].size()) {
				string timeWord = words[index + 1];
				words[index + 1] = words[index];
				words[index] = timeWord;
			}
		}
	}

	ofstream writeFile;
	writeFile.open("output.txt", ofstream::out | ofstream::trunc); //очистив, открываем файл на запись

	for (short int wordsInd = 0; wordsInd < 100; wordsInd++) { //записываем спсиок найденных слов в файл
		if (words[wordsInd] != "")
			writeFile << words[wordsInd] << "\n";
	}

	writeFile.close();

	cout << "Words: " << "\n";
	for (short int w = 0; w < 100; w++) {
		if (words[w] != "")
			cout << words[w] << "\n";
	}
	delete[] words;

	system("pause");
	return 0;


}
