#define   Name       "RombusCounter"
;  
#define   Version    "0.0.1"
; -
#define   Publisher  "IlyaLvov"
;   
#define   ExeName    "Rombus.exe"

[Setup]

;   , 
;  Tools -> Generate GUID
AppId={{A5D71F70-C0D2-4079-BEFF-4EA5826FEB84}

;  ,   
AppName={#Name}
AppVersion={#Version}
AppPublisher={#Publisher}

;   -
DefaultDirName={pf}\{#Name}
;     ""
DefaultGroupName={#Name}

; ,     setup    
OutputDir=F:\Rombus
OutputBaseFileName=rombus-setup

;  
SetupIconFile=F:\Rombus\icon.ico

;  
Compression=lzma
SolidCompression=yes

[Tasks]
;     
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]

;  
Source: "C:\Users\Ilya\Documents\ProgrammsC\LWP\LWP\Debug\ADD_8.exe"; DestDir: "{app}"; Flags: ignoreversion

[Icons]

Name: "{group}\{#Name}"; Filename: "{app}\{#ExeName}"

Name: "{commondesktop}\{#Name}"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon
