﻿#include <iostream>
bool VisYearDefine(int year) {
	if ((year % 4 == 0) && (year % 100 != 0)) {
		return 1;
	}
	else if (year % 400 == 0)
		return 1;
	else
		return 0;
}
int main()
{
	int year;
	std::cin >> year;

	while (year < 2016) {
		if (VisYearDefine(year)) {
			year += 28;
		}
		else if (VisYearDefine(year - 1)) {
			year += 6;
		}
		else {
			year += 11;
		}
	}

	std::cout << year;
}