﻿#include <iostream>

short int NumSumm(int num) { //ф-ция суммирования цифр числа
    short int summ = 0;
    while (num != 0) {
        summ += num % 10;
        num = num / 10;
    }
    return summ;
}

int main()
{
    unsigned int count = 0; //счётчик
    for (long int i = 1000; i <= 999999; i++) {
        if (NumSumm(i / 1000) == NumSumm(i % 1000)) //сравниваем суммы цифр левой и правой половины
            count++;
    }
    std::cout << count + 1; //выводим значение счётчика + билет "000000"
}
