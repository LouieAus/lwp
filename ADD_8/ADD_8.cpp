﻿#include <iostream>
#include "figure.h"

int main()
{
	setlocale(LC_ALL, "Rus");
	figures::IsoTrinagle triangle(10, 5, 10);
	std::cout << "Периметр: " << triangle.PerimetrFind() << '\n';
	std::cout << "Площадь: " << triangle.SquareFind() << '\n';
}