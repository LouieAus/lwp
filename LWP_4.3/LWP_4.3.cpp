﻿#include <iostream>
using namespace std;

int main() {
	unsigned short int n, m;
	int max_mult = 0;
	unsigned short int maxmult_index;

	cout << "Enter number of stroke quantity: ";
	cin >> n;
	cout << "Enter number of column quantity: ";
	cin >> m;

	unsigned int** arr = new unsigned int* [n];
	for (unsigned short int x = 0; x < n; x++) arr[x] = new unsigned int[m];

	cout << "Enter values: " << '\n';
	//Вводим значения элементов матрицы
	for (unsigned short int x = 0; x < n; x++)
		for (unsigned short int y = 0; y < m; y++) {
			cout << "(" << x + 1 << ", " << y + 1 << "): ";
			cin >> arr[x][y];
		}

	//Поиск столбца с наибольшим произведением элементов
	for (unsigned short y = 0; y < m; y++) {
		unsigned long int mult = 1;
		for (unsigned short int x = 0; x < n; x++) {
			mult = mult * arr[x][y];
		}
		if (mult > max_mult) {
			max_mult = mult;
			maxmult_index = y;
		}
	}

	//Уменьшаем значения элементов найденного столбца на 3
	for (unsigned short i = 0; i < n; i++) {
		arr[i][maxmult_index] = arr[i][maxmult_index] - 3;
	}

	//Вывод матрицы
	for (unsigned short x = 0; x < n; x++) {
		cout << "Stroke " << x + 1 << ": ";
		for (unsigned short y = 0; y < m; y++) {
			cout << arr[x][y] << " ";
		}
		cout << '\n';
	}

	//Чистим память
	for (unsigned short int x = 0; x < n; x++) delete[] arr[x];

	system("pause");
	return 0;
}
