#pragma once
#include <iostream>
namespace vector {
	class Vector {

	private:
		int size;
		std::string type;
		int* Vector_int;
		float* Vector_float;
		char* Vector_char;

	public:
		Vector(int arrSize, std::string arrType);
		~Vector();
		void Init(int arrSize, std::string arrType);
		int GetSize();
		void Add(int INT, float FLOAT, char CHAR, int index);
		int GetItem_Int(int index);
		bool Check_Int();
	};

}