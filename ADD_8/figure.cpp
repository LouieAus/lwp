#include <iostream>
#include <math.h>
#include "figure.h"
namespace figures {
	IsoTrinagle::IsoTrinagle(float a_side, float b_side, float c_side) {
			Init(a_side, b_side, c_side);
	}
	void IsoTrinagle::Init(float a_side, float b_side, float c_side) {
			if (((a_side == b_side) || (b_side == c_side) || (a_side == c_side))
				&& (a_side > 0 && b_side > 0 && c_side > 0)) {
				a = a_side;
				b = b_side;
				c = c_side;
			}
			else {
				std::cout << "�������� ������! ������ ���� ������ ������������." << '\n';
				a = 1;
				b = 1;
				c = 1;
			}
	}
	float IsoTrinagle::PerimetrFind() {
			return a + b + c;
	}
	float IsoTrinagle::SquareFind() {
			float halfP = PerimetrFind() / 2;
			float square = sqrt(halfP * (halfP - a) * (halfP - b) * (halfP - c));
			return square;
	}
}