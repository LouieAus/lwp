﻿#include <iostream>

#include "vector.h"

int main(){
    vector::Vector arr_1(2, "int");
    arr_1.Add(50, 0, '0', 0);
    arr_1.Add(100, 0, '0', 1);
    std::cout << arr_1.GetItem_Int(0) << '\n';
}