#include <SFML/Graphics.hpp>
#include "class.h"

namespace forms {
	Rectangles::Rectangles(size Size, coord Coord, color Color, float Speed) {
		RectInit(Size, Coord, Color, Speed, rect);
	}
	void Rectangles::RectInit(size Size, coord Coord, color Color, float Speed, sf::RectangleShape &rectan) {
		rectSize = Size;
		rectCoord = Coord;
		rectColor = Color;
		rectSpeed = Speed;
		rectan = Create(rectSize, rectCoord, rectColor, rectSpeed);
	}
	sf::RectangleShape Rectangles::Create(size S, coord C, color Cl, float Sp) {
		sf::RectangleShape rect(sf::Vector2f(S.wide, S.high));
		rect.setFillColor(sf::Color(Cl.r, Cl.g, Cl.b));
		rect.setPosition(C.x, C.y);
		return rect;
	}
	void Rectangles::PosChange() {
		if (rectCoord.y > 0) {
			rectCoord.y += rectSpeed;
			rect.setPosition(sf::Vector2f(rectCoord.x, rectCoord.y));
		}
	}
}